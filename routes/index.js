var express = require('express');
var router = express.Router();

router.get('/', function(req, res, next) {
  res.render('pages/home', { });
});

router.get('/success', function(req, res, next) {
  res.render('pages/success', { });
});

module.exports = router;
